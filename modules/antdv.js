/* ant design vue 模块 */

/**
 * @name: convertData2Tree
 * @cname: 树形数据转换
 * @desc: 将数据转换成a-tree所需格式
 * @param {*} data keyField key字段 默认id titleField title字段 默认name
 * @panme: var arr = [{id:'aa',name:'0',children:[{id:'aa',name:"1",children:[{id:'aa',name:"2"}]}]}];
 * @result: [{"title": "0","key": "aa","children": [{"title": "1","key": "aa","children": [{"title": "2","key": "aa"}]}]}]
 */
export function convertData2Tree(data, keyField = 'id', titleField = 'name') {
    return data.map((r) => {
        let t = {
            title: r[titleField],
            key: r[keyField]
        };
        if (r.children)
            t.children = convertData2Tree(r.children, keyField, titleField);
        return t;
    });
}