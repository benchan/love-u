let fs = require('fs')
let res = fs.readFileSync('../dist/iloveu.esm.js').toString()
let templatePath = './template.html'
let template = fs.readFileSync(templatePath).toString()

let html = res.match(/function\s+\w+/g).map(r => r.replace(/function\s+(\w+)/, '$1'))
    .map(r => `<h3>${r}</h3>`).join('')

fs.writeFileSync('../doc.html', template.replace('$CONTENT$', html))

console.log('生成文档成功');